package net.crunchdroid.recipes.crunch.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrunchCmsBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrunchCmsBootApplication.class, args);
    }
}
